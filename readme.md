
## Hardware

Assumes you have a pi (likely a zero) with a [waveshare 1.3in LCD Hat](https://www.waveshare.com/wiki/1.3inch_LCD_HAT)

## Software

### Setup

#### Config

1) `cp config-example.json config.json`
2) edit config.json

#### Service

1) `sudo cp clubhouse.service /lib/systemd/system/`
2) `sudo chmod 644 /lib/systemd/system/clubhouse.service`
3) `sudo systemctl daemon-reload`
4) `sudo systemctl enable clubhouse.service`
5) `sudo systemctl start clubhouse.service`

### Notes


 * https://github.com/PiSugar/pisugar-case-pihat-cap

pigpio
