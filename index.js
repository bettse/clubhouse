const fs = require("fs");
const { spawn } = require("child_process");
const WebSocket = require("ws");
const LCD = require("@bettse/ws13lcd");
const Controller = require("./Controller");

const { Colors } = LCD;
const { BLACK, WHITE, RED, GREEN, BLUE, MAGENTA, CYAN, YELLOW, GRAY } = Colors;

const DECIMAL = 10;
const font = { xs: 8, sm: 12, md: 16, lg: 20, xl: 24 };
const speed = {
  hyper: 100,
  fast: 300,
  slow: 1000,
  snail: 5000,
}

const fontRatio = {
  [font.xs]: 5,
  [font.sm]: 7,
  [font.md]: 11,
  [font.lg]: 14,
  [font.xl]: 17,
}

process.on("SIGINT", () => {
  console.log('SIGINT');
  LCD.clear(BLACK);
  LCD.exit();
  process.exit();
});

const SCREEN = {
  HEIGHT: LCD.HEIGHT - font.xl,
  WIDTH: LCD.WIDTH - fontRatio[font.xl],
}

function drawText(x, y, s, f = font.md, fore = WHITE, back = BLACK) {
  LCD.drawString(x, y, s, f, fore, back);
}

/*
// If you want labels to line up with buttons on the right:
drawTextRightJustify(40 + font.xl/2, "one-", font.xl, RED)
drawTextRightJustify(100 + font.xl/2, "two-", font.xl, RED)
drawTextRightJustify(160 + font.xl/2, "three-", font.xl, RED)
*/

function drawTextRightJustify(y, s, f = font.md, fore = WHITE, back = BLACK) {
  const charWidth = fontRatio[f];
  const x = LCD.WIDTH - charWidth * s.length;
  drawText(x, y, s, f, fore, back)
}

function str2Const(str) {
  const map = {
    'black': BLACK,
    'white': WHITE,
    'red': RED,
    'green': GREEN,
    'blue': BLUE,
    'magenta': MAGENTA,
    'cyan': CYAN,
    'yellow': YELLOW,
    'gray': GRAY,
  }
  return map[str.toLowerCase()] || WHITE;
}


function displayCommands(commands, selected) {
  const fontSize = font.lg;
  const maxChar = Math.floor(SCREEN.WIDTH / fontRatio[fontSize]);
  const x = 0;
  const maxLines = Math.floor(SCREEN.HEIGHT / fontSize);
  let start = 0;
  let end = maxLines; // The end line isn't shown

  if (selected >= end) {
    start = selected - end + 1;
    end = start + maxLines;
  }
  // console.log({maxLines, start, selected, end});

  LCD.draw(() => {
    LCD.clear(BLACK);

    if (start > 0) {
      drawTextRightJustify(0, start.toString(), font.md, RED);
    }
    if (end < commands.length) {
      drawTextRightJustify(SCREEN.HEIGHT - font.md, (commands.length - end).toString(), font.md, RED);
    }

    commands.slice(start, end).forEach((command, i) => {
      const { color = "white", display } = command;
      const y = i * fontSize;
      const fore = str2Const(color);

      if ((start + i) === selected) {
        // Invert selected
        drawText(x, y, display.substring(0, maxChar), fontSize, BLACK, fore);
      } else {
        drawText(x, y, display.substring(0, maxChar), fontSize, fore);
      }
    });

    drawStatusLine();
  });
}

var charging = false;
var percent = 0;
function drawStatusLine() {
  if (percent < 50) {
    drawTextRightJustify(SCREEN.HEIGHT, `${percent}%`, font.xl, RED);
  } else {
    drawTextRightJustify(SCREEN.HEIGHT, `${percent}%`, font.xl);
  }
  if (charging) {
    drawText(0, SCREEN.HEIGHT, 'Charging', font.xl);
  } else {
    drawText(0, SCREEN.HEIGHT, '        ', font.xl);
  }
}

function batteryMonitor() {
  const ws = new WebSocket("ws://localhost:8421/ws");

  ws.on("open", function open() {
    ws.send("get battery");
  });

  ws.on("message", function incoming(data) {
    if (data.startsWith("battery: ")) {
      percent = parseInt(data.slice("battery: ".length), DECIMAL);
    } else if (data.startsWith("battery_power_plugged: ")) {
      charging = JSON.parse(data.slice("battery_power_plugged: ".length));
    } else if (data.length > 0) {
      log.err("WS message:", data);
    }
    drawStatusLine();
  });

  setInterval(() => {
    ws.send("get battery");
    ws.send("get battery_power_plugged");
  }, speed.slow);
}

function main() {
  const controller = new Controller();
  let updating = null;
  let selected = 0;
  let process = null;

  LCD.init();
  LCD.clear(BLACK);
  const configJson = fs.readFileSync('config.json', 'utf8');
  const config = JSON.parse(configJson);
  // TODO: add other things to config: font size, ?
  const { pisugar2, commands } = config;

  if(pisugar2) {
    batteryMonitor();
  }

  controller.on("up", () => {
    selected = (selected - 1 + commands.length) % commands.length
    displayCommands(commands, selected);
  });

  controller.on("down", () => {
    selected = (selected + 1 + commands.length) % commands.length
    displayCommands(commands, selected);
  });

  controller.on("press", () => {});

  controller.on("left", () => {});

  controller.on("right", () => {});

  controller.on("button", (button) => {
    console.log({ button });
    if (["1", "2", "3"].indexOf(button) === -1) {
      return;
    }

    clearInterval(updating);
    updating = null;
    LCD.clear(BLACK);
    const fontSize = font.xl;
    const command = commands[selected];
    const { display } = command;
    drawText(0, 0, `Running ${display}#${button}`, fontSize);
    const [first, ...rest] = command[button].split(' ');


    // Special case
    if (button === "3" && first === "kill") {
      process.kill();
      return;
    }

    process = spawn(first, rest);

    process.stdout.on("data", data => {
        console.log(`stdout: ${data}`);
    });

    process.stderr.on("data", data => {
        console.log(`stderr: ${data}`);
    });

    process.on("close", code => {
      if (code) {
        console.log(`child process exited with code ${code}`);
      }
      displayCommands(commands, selected);
    });
    process.on("error", error => {
      console.log('error running command', error);
      displayCommands(commands, selected);
    })
  });

  controller.on("1", async () => {
  });

  controller.on("2", async () => {
  });

  controller.on("3", async () => {
  });

  displayCommands(commands, selected);
}

main()
